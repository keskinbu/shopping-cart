# Shopping Cart

```
docker-compose up -d
```

## Examples
```
## Category - Add
curl -X "POST" "http://localhost:8080/category/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "Giyim"
}'

## Category - Add
curl -X "POST" "http://localhost:8080/category/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "T-Shirt",
  "parent": {
    "id": 1
  }
}'


## Category - Add
curl -X "POST" "http://localhost:8080/category/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "Ayakkabi"
}'



## Product - Add
curl -X "POST" "http://localhost:8080/product/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "Gomlek",
  "price": "60.00",
  "category": {
    "id": 1
  }
}'

## Product - Add
curl -X "POST" "http://localhost:8080/product/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "Siyah T-Shirt",
  "price": "40.00",
  "category": {
    "id": 2
  }
}'

## Product - Add
curl -X "POST" "http://localhost:8080/product/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "Beyaz T-Shirt",
  "price": "30.00",
  "category": {
    "id": 2
  }
}'

## Product - Add
curl -X "POST" "http://localhost:8080/product/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "Spor Ayakkabi",
  "price": "120.00",
  "category": {
    "id": 3
  }
}'

## Product - Add
curl -X "POST" "http://localhost:8080/product/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "Sneaker",
  "price": "150.00",
  "category": {
    "id": 3
  }
}'

## Product - List
curl "http://localhost:8080/product/list" \
     -H 'Content-Type: application/json'



## Coupon - Add
curl -X "POST" "http://localhost:8080/coupon/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "code": "IND10",
  "discount": 10,
  "discountType": "RATE",
  "minAmount": 100
}'

## Coupon - Add
curl -X "POST" "http://localhost:8080/coupon/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "code": "IND50",
  "discount": 50,
  "discountType": "RATE",
  "minAmount": 500
}'

## Coupon - Add
curl -X "POST" "http://localhost:8080/coupon/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "code": "IND10TL",
  "discount": 10,
  "discountType": "PRICE",
  "minAmount": 100
}'



## Campaign - Add
curl -X "POST" "http://localhost:8080/campaign/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "CAMP10_TShirt",
  "discount": 10,
  "discountType": "RATE",
  "category": {
    "id": 2
  },
  "minProductCount": 3
}'

## Campaign - Add
curl -X "POST" "http://localhost:8080/campaign/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "CAMP20_Ayakkabi",
  "discount": 20,
  "discountType": "RATE",
  "category": {
    "id": 3
  },
  "minProductCount": 4
}'

## Campaign - Add
curl -X "POST" "http://localhost:8080/campaign/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "title": "CAMP100TL_TShirt",
  "discount": 100,
  "discountType": "PRICE",
  "category": {
    "id": 3
  },
  "minProductCount": 5
}'



## Cart - Create
curl -X "POST" "http://localhost:8080/cart/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "productCount": 2,
  "productId": 2
}'

## Cart - Add Same Product
curl -X "POST" "http://localhost:8080/cart/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "cartId": 1,
  "productCount": 1,
  "productId": 2
}'

## Cart - Add Different Product
curl -X "POST" "http://localhost:8080/cart/add" \
     -H 'Content-Type: application/json' \
     -d $'{
  "cartId": 1,
  "productCount": 4,
  "productId": 3
}'


## Cart - Show
curl "http://localhost:8080/cart/1" \
     -H 'Content-Type: application/json'


## Cart - Print
curl "http://localhost:8080/cart/1/print" \
     -H 'Content-Type: application/json'

```