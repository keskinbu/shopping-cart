package com.trendyol.shoppingcart.service;


import com.trendyol.shoppingcart.model.Category;

public interface CategoryService {
    boolean addCategory(Category category);
}
