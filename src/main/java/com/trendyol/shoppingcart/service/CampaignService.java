package com.trendyol.shoppingcart.service;


import com.trendyol.shoppingcart.model.Campaign;

import java.util.List;

public interface CampaignService {
    boolean addCampaign(Campaign campaign);

    List<Campaign> getCampaigns(Long categoryId, int quantity);
}
