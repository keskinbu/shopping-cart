package com.trendyol.shoppingcart.service;


import com.trendyol.shoppingcart.model.Coupon;

import java.math.BigDecimal;
import java.util.List;

public interface CouponService {
    boolean addCoupon(Coupon coupon);

    List<Coupon> getCouponsByAmount(BigDecimal amount);
}
