package com.trendyol.shoppingcart.service;


import com.trendyol.shoppingcart.model.Cart;
import com.trendyol.shoppingcart.model.dto.CartDto;
import com.trendyol.shoppingcart.model.dto.ItemDto;

public interface CartService {
    Cart addItem(ItemDto item);

    Cart getCart(Long id);

    CartDto print(Long id);
}
