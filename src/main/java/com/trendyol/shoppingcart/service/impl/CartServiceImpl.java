package com.trendyol.shoppingcart.service.impl;

import com.trendyol.shoppingcart.dao.CartRepository;
import com.trendyol.shoppingcart.dao.ItemRepository;
import com.trendyol.shoppingcart.dao.ProductRepository;
import com.trendyol.shoppingcart.model.*;
import com.trendyol.shoppingcart.model.dto.CartDto;
import com.trendyol.shoppingcart.model.dto.CategoryDto;
import com.trendyol.shoppingcart.model.dto.ItemDto;
import com.trendyol.shoppingcart.model.dto.ProductDto;
import com.trendyol.shoppingcart.service.CampaignService;
import com.trendyol.shoppingcart.service.CartService;
import com.trendyol.shoppingcart.service.CouponService;
import com.trendyol.shoppingcart.service.DeliveryService;
import com.trendyol.shoppingcart.util.NumberUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {
    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private CouponService couponService;

    @Autowired
    private DeliveryService deliveryService;

    @Autowired
    public CartServiceImpl(DeliveryService deliveryService, CampaignService campaignService, CouponService couponService, CartRepository cartRepository, ProductRepository productRepository) {
        this.cartRepository = cartRepository;
        this.deliveryService = deliveryService;
        this.campaignService = campaignService;
        this.couponService = couponService;
        this.productRepository = productRepository;
    }

    @Override
    public Cart addItem(ItemDto item) {
        Long cartId = item.getCartId();
        Long productId = item.getProductId();
        Cart cart = new Cart();

        boolean onlyProductCountUpdate = false;

        Product product = productRepository.findById(productId).orElse(null);

        if (!NumberUtils.isZero(cartId)) {
            cart = cartRepository.findById(cartId).orElse(cart);
            onlyProductCountUpdate = updateAndCheckSameProductQuantity(item, productId, cart);
        }

        BigDecimal productTotalPrice = product.getPrice().multiply(new BigDecimal(item.getProductCount()));
        cart.setTotalAmount(cart.getTotalAmount().add(productTotalPrice));

        if (!onlyProductCountUpdate) {
            Item newItem = createItemFromDto(item, cart, product);
            cart.getItems().add(newItem);
        }

        applyCampaign(cart);
        applyCoupon(cart);
        deliveryService.addDeliveryCost(cart);

        cartRepository.save(cart);

        return cart;
    }

    @Override
    public Cart getCart(Long id) {
        Cart cart = cartRepository.findById(id).orElse(new Cart());

        return cart;
    }

    @Override
    public CartDto print(Long id) {
        Cart cart = getCart(id);

        Map<String, Map<Long, Integer>> categoryMap = new HashMap<>();

        CartDto cartDto = new CartDto();

        for (Item item : cart.getItems()) {
            Product product = item.getProduct();
            String categoryTitle = product.getCategory().getTitle();

            Long productId = product.getId();
            int quantity = item.getQuantity();

            if (categoryMap.containsKey(categoryTitle)) {
                Map<Long, Integer> productMap = categoryMap.get(categoryTitle);
                if (productMap.containsKey(productId)) {
                    Integer productCount = productMap.get(productId);
                    productMap.put(productId, productCount + quantity);
                } else {
                    productMap.put(productId, quantity);
                }

                categoryMap.put(categoryTitle, productMap);
            } else {
                Map<Long, Integer> productMap = new HashMap<>();
                productMap.put(productId, quantity);
                categoryMap.put(categoryTitle, productMap);
            }
        }

        List<CategoryDto> categories = new ArrayList<>();

        categoryMap.forEach((title, productMap) -> {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setCategoryName(title);
            categoryDto.setProducts(prepareProductDto(productMap));

            categories.add(categoryDto);
        });


        cartDto.setCategories(categories);
        cartDto.setTotalPrice(cart.getTotalAmountAfterDiscount());
        cartDto.setTotalDiscount(cart.getCouponDiscount().add(cart.getCampaignDiscount()));
        cartDto.setDeliveryCost(cart.getDeliveryCost());

        return cartDto;
    }

    private List<ProductDto> prepareProductDto(Map<Long, Integer> productMap) {
        List<ProductDto> productDtos = new ArrayList<>();

        productMap.forEach((id, quantity) -> {
            Product product = productRepository.findById(id).orElse(null);
            if (product != null) {
                ProductDto productDto = new ProductDto();

                productDto.setQuantity(quantity);
                productDto.setTitle(product.getTitle());
                productDto.setUnitPrice(product.getPrice());
                productDto.setTotalPrice(product.getPrice().multiply(new BigDecimal(quantity)));

                productDtos.add(productDto);
            }

        });

        return productDtos;
    }

    private boolean updateAndCheckSameProductQuantity(ItemDto item, Long productId, Cart cart) {
        List<Item> items = cart.getItems();
        if (!CollectionUtils.isEmpty(items)) {
            for (Item cartItem : items) {
                if (cartItem.getProduct().getId().equals(productId)) {
                    cartItem.setQuantity(cartItem.getQuantity() + item.getProductCount());
                    return true;
                }
            }
        }

        return false;
    }

    private Item createItemFromDto(ItemDto item, Cart cart, Product product) {
        Item newItem = new Item();
        newItem.setCart(cart);
        newItem.setProduct(product);
        newItem.setQuantity(item.getProductCount());

        return newItem;
    }

    private void applyCampaign(Cart cart) {
        BigDecimal totalAmount = cart.getTotalAmount();

        BigDecimal totalProductPrice = new BigDecimal(0.0);

        List<Item> items = cart.getItems();
        if (!CollectionUtils.isEmpty(items)) {
            for (Item item : items) {
                Product product = item.getProduct();

                BigDecimal productPrice = product.getPrice().multiply(new BigDecimal(item.getQuantity()));

                BigDecimal minProductPrice = productPrice;

                List<Campaign> campaigns = campaignService.getCampaigns(product.getCategory().getId(), item.getQuantity());
                if (!CollectionUtils.isEmpty(campaigns)) {
                    for (Campaign campaign : campaigns) {
                        BigDecimal total = calculateTotalAmountAfterDiscount(campaign.getDiscountType(), campaign.getDiscount(), productPrice);
                        if (total.compareTo(minProductPrice) < 0) {
                            minProductPrice = total;
                        }
                    }
                }

                totalProductPrice = totalProductPrice.add(minProductPrice);
            }
        }

        cart.setTotalAmountAfterDiscount(totalProductPrice);
        cart.setCampaignDiscount(totalAmount.subtract(totalProductPrice));
    }

    private void applyCoupon(Cart cart) {
        BigDecimal totalAmount = cart.getTotalAmountAfterDiscount();
        BigDecimal minTotal = totalAmount;

        List<Coupon> coupons = couponService.getCouponsByAmount(totalAmount);

        if (!CollectionUtils.isEmpty(coupons)) {
            for (Coupon coupon : coupons) {
                BigDecimal total = calculateTotalAmountAfterDiscount(coupon.getDiscountType(), coupon.getDiscount(), totalAmount);

                if (minTotal.compareTo(total) > 0) {
                    minTotal = total;
                }
            }
        }

        cart.setTotalAmountAfterDiscount(minTotal);
        cart.setCouponDiscount(totalAmount.subtract(cart.getTotalAmountAfterDiscount()));
    }

    private BigDecimal calculateTotalAmountAfterDiscount(DiscountType discountType, BigDecimal discount, BigDecimal price) {
        BigDecimal total = new BigDecimal(0.0);

        switch (discountType) {
            case PRICE:
                total = price.subtract(discount);
                break;
            case RATE:
                total = price.subtract(price.multiply(discount).divide(new BigDecimal(100)));
                break;
        }

        return total;
    }
}
