package com.trendyol.shoppingcart.service.impl;

import com.trendyol.shoppingcart.dao.CategoryRepository;
import com.trendyol.shoppingcart.exception.ShoppingCartException;
import com.trendyol.shoppingcart.model.Category;
import com.trendyol.shoppingcart.service.CategoryService;
import com.trendyol.shoppingcart.util.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public boolean addCategory(Category category) {
        validateCategory(category);

        if (category.getParent() != null && !NumberUtils.isZero(category.getParent().getId())) {
            Category parentCategory = categoryRepository.findById(category.getParent().getId()).orElse(null);
            category.setParent(parentCategory);
        }

        categoryRepository.save(category);

        return true;
    }

    private boolean validateCategory(Category category) {
        String title = category.getTitle();

        Category result = categoryRepository.findByTitle(title);

        if (result != null) {
            throw new ShoppingCartException(61, "Category name already exist");
        }

        return true;
    }
}
