package com.trendyol.shoppingcart.service.impl;

import com.trendyol.shoppingcart.model.Cart;
import com.trendyol.shoppingcart.util.DeliveryCostCalculator;
import com.trendyol.shoppingcart.service.DeliveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class DeliveryServiceImpl implements DeliveryService {
    private static final BigDecimal COST_PER_DELIVERY = new BigDecimal(5.00);
    private static final BigDecimal COST_PER_PRODUCT = new BigDecimal(1.00);
    private static final BigDecimal FIXED_COST = new BigDecimal(2.99);

    @Override
    public void addDeliveryCost(Cart cart) {
        if (!CollectionUtils.isEmpty(cart.getItems())) {
            DeliveryCostCalculator deliveryCostCalculator = new DeliveryCostCalculator(COST_PER_DELIVERY, COST_PER_PRODUCT, FIXED_COST);

            deliveryCostCalculator.calculateFor(cart);
        }
    }
}
