package com.trendyol.shoppingcart.service.impl;

import com.trendyol.shoppingcart.dao.CouponRepository;
import com.trendyol.shoppingcart.model.Coupon;
import com.trendyol.shoppingcart.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {
    @Autowired
    private CouponRepository couponRepository;

    @Override
    public boolean addCoupon(Coupon coupon) {
        if (!isValid(coupon)) {
            return false;
        }

        couponRepository.save(coupon);

        return true;
    }

    @Override
    public List<Coupon> getCouponsByAmount(BigDecimal amount) {
        return couponRepository.findByMinAmountLessThanEqual(amount);
    }

    private boolean isValid(Coupon coupon) {
        Coupon result = couponRepository.findByCode(coupon.getCode());

        if (result != null) {
            return false;
        }

        return true;
    }
}
