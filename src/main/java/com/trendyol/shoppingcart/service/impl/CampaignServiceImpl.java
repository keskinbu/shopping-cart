package com.trendyol.shoppingcart.service.impl;

import com.trendyol.shoppingcart.dao.CampaignRepository;
import com.trendyol.shoppingcart.model.Campaign;
import com.trendyol.shoppingcart.service.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CampaignServiceImpl implements CampaignService {
    @Autowired
    private CampaignRepository campaignRepository;

    @Override
    public boolean addCampaign(Campaign campaign) {
        if (!isValid(campaign)) {
            return false;
        }

        campaignRepository.save(campaign);

        return true;
    }

    @Override
    public List<Campaign> getCampaigns(Long categoryId, int quantity) {
        return campaignRepository.findByCategoryIdAndMinProductCountLessThanEqual(categoryId, quantity);
    }

    private boolean isValid(Campaign campaign) {
        Campaign result = campaignRepository.findByTitle(campaign.getTitle());

        if (result != null) {
            return false;
        }

        return true;
    }
}
