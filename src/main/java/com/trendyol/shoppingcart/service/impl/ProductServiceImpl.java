package com.trendyol.shoppingcart.service.impl;

import com.trendyol.shoppingcart.dao.ProductRepository;
import com.trendyol.shoppingcart.model.Product;
import com.trendyol.shoppingcart.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    @Override
    public boolean addProduct(Product product) {
        if (!isValid(product)) {
            return false;
        }

        productRepository.save(product);

        return true;
    }

    private boolean isValid(Product product) {
        Product result = productRepository.findByTitle(product.getTitle());

        if (result != null) {
            return false;
        }

        return true;
    }
}
