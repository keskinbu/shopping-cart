package com.trendyol.shoppingcart.service;

import com.trendyol.shoppingcart.model.Cart;

public interface DeliveryService {
    void addDeliveryCost(Cart cart);
}
