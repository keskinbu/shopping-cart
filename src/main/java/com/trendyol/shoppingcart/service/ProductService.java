package com.trendyol.shoppingcart.service;


import com.trendyol.shoppingcart.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> getProducts();

    boolean addProduct(Product product);
}
