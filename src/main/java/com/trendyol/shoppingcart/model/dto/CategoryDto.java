package com.trendyol.shoppingcart.model.dto;

import com.trendyol.shoppingcart.model.Product;

import java.util.List;

public class CategoryDto {
    private List<ProductDto> products;

    private String categoryName;

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
