package com.trendyol.shoppingcart.model.dto;

import com.trendyol.shoppingcart.model.Product;

import java.math.BigDecimal;
import java.util.List;

public class ProductDto {
    private String title;

    private Integer quantity;

    private BigDecimal unitPrice;

    private BigDecimal totalPrice;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
