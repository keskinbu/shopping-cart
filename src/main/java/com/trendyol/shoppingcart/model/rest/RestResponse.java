package com.trendyol.shoppingcart.model.rest;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(
        fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class RestResponse<T> {
    private T data;
    private ErrorResponse error;
    private boolean valid = true;

    public RestResponse() {
    }

    public RestResponse(T data) {
        this.data = data;
        this.valid = true;
    }

    public RestResponse(T data, boolean valid) {
        this.data = data;
        this.valid = valid;
    }

    public RestResponse(T data, ErrorResponse error, boolean valid) {
        this.data = data;
        this.valid = valid;
        this.error = error;
    }

    public Object getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("Response");
        sb.append("{data=").append(data);
        sb.append(", valid=").append(valid);
        sb.append(", error=").append(error);
        sb.append('}');
        return sb.toString();
    }
}
