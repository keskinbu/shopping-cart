package com.trendyol.shoppingcart.model;

public enum DiscountType {
    PRICE,
    RATE
}
