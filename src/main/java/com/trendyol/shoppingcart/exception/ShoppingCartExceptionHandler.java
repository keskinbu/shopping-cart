package com.trendyol.shoppingcart.exception;

import com.trendyol.shoppingcart.model.rest.ErrorResponse;
import com.trendyol.shoppingcart.model.rest.RestResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@ControllerAdvice
@RestControllerAdvice
public class ShoppingCartExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public RestResponse<Object> serviceExceptionHandler(ShoppingCartException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(ex.getErrorCode());
        error.setMessage(ex.getMessage());
        return new RestResponse(null, error, false);
    }
}
