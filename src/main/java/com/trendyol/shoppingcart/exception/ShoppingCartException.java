package com.trendyol.shoppingcart.exception;

public class ShoppingCartException extends RuntimeException {
    private int errorCode;
    private String message;
    private StackTraceElement[] stackTraceElement;
    private Throwable cause;


    public ShoppingCartException(int code, String message) {
        super(message);
        this.errorCode = code;
        this.message = message;
        this.stackTraceElement = getStackTrace();
        this.cause = getCause();
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StackTraceElement[] getStackTraceElement() {
        return stackTraceElement;
    }

    public void setStackTraceElement(StackTraceElement[] stackTraceElement) {
        this.stackTraceElement = stackTraceElement;
    }

    @Override
    public Throwable getCause() {
        return cause;
    }

    public void setCause(Throwable cause) {
        this.cause = cause;
    }
}
