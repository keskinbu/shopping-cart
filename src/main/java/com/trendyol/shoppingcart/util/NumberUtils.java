package com.trendyol.shoppingcart.util;

public class NumberUtils {
    public static boolean isZero(Long value) {
        return value == null || value.equals(0L);
    }
}
