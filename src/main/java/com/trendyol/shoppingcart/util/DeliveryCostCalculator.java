package com.trendyol.shoppingcart.util;

import com.trendyol.shoppingcart.model.Cart;
import com.trendyol.shoppingcart.model.Item;
import com.trendyol.shoppingcart.model.Product;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeliveryCostCalculator {
    private BigDecimal costPerDelivery;

    private BigDecimal costPerProduct;

    private BigDecimal fixedCost;

    public DeliveryCostCalculator(BigDecimal costPerDelivery, BigDecimal costPerProduct, BigDecimal fixedCost) {
        this.costPerDelivery = costPerDelivery;
        this.costPerProduct = costPerProduct;
        this.fixedCost = fixedCost;
    }

    public BigDecimal calculateCost(int categoryCount, int productCount) {
        return (this.costPerDelivery.multiply(new BigDecimal(categoryCount)))
                .add(this.costPerProduct.multiply(new BigDecimal(productCount)))
                .add(this.fixedCost);
    }

    public void calculateFor(Cart cart) {
        List<Item> items = cart.getItems();

        Map<Long, Integer> productMap = new HashMap<>();
        Map<Long, Integer> categoryMap = new HashMap<>();

        for (Item item : items) {
            Product product = item.getProduct();

            Long productId = product.getId();
            Long categoryId = product.getCategory().getId();

            createDistinctMap(productMap, productId);
            createDistinctMap(categoryMap, categoryId);
        }

        BigDecimal deliveryCost = calculateCost(categoryMap.size(), productMap.size());

        cart.setDeliveryCost(deliveryCost);
    }

    private void createDistinctMap(Map<Long, Integer> map, Long id) {
        if (map.containsKey(id)) {
            Integer count = map.get(id);
            map.put(id, ++count);
        } else {
            map.put(id, 1);
        }
    }
}
