package com.trendyol.shoppingcart.dao;

import com.trendyol.shoppingcart.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {
    List<Item> findByCart(Long cartId);
}
