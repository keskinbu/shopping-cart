package com.trendyol.shoppingcart.dao;

import com.trendyol.shoppingcart.model.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CampaignRepository extends JpaRepository<Campaign, Long> {
    Campaign findByTitle(String title);

    List<Campaign> findByCategoryIdAndMinProductCountLessThanEqual(Long categoryId, int minProductCount);
}
