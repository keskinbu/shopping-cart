package com.trendyol.shoppingcart.dao;

import com.trendyol.shoppingcart.model.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;

public interface CouponRepository extends JpaRepository<Coupon, Long> {
    Coupon findByCode(String code);

    List<Coupon> findByMinAmountLessThanEqual(BigDecimal amount);
}
