package com.trendyol.shoppingcart.controller;

import com.trendyol.shoppingcart.model.Cart;
import com.trendyol.shoppingcart.model.dto.CartDto;
import com.trendyol.shoppingcart.model.dto.ItemDto;
import com.trendyol.shoppingcart.model.rest.RestResponse;
import com.trendyol.shoppingcart.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;

    @PostMapping("/add")
    public RestResponse<Cart> addItem(@RequestBody ItemDto itemDto) {
        return new RestResponse<>(cartService.addItem(itemDto));
    }

    @GetMapping("/{id}")
    public RestResponse<Cart> getCart(@PathVariable Long id) {
        return new RestResponse<Cart>(cartService.getCart(id));
    }

    @GetMapping("/{id}/print")
    public RestResponse<CartDto> print(@PathVariable Long id) {
        return new RestResponse<CartDto>(cartService.print(id));
    }
}
