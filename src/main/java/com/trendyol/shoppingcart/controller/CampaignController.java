package com.trendyol.shoppingcart.controller;

import com.trendyol.shoppingcart.model.Campaign;
import com.trendyol.shoppingcart.model.rest.RestResponse;
import com.trendyol.shoppingcart.service.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/campaign")
public class CampaignController {
    @Autowired
    private CampaignService campaignService;

    @PostMapping("/add")
    public RestResponse<Boolean> addCampaign(@RequestBody Campaign campaign) {
        boolean response = campaignService.addCampaign(campaign);
        return new RestResponse<>(response);
    }
}
