package com.trendyol.shoppingcart.controller;

import com.trendyol.shoppingcart.model.Campaign;
import com.trendyol.shoppingcart.model.Coupon;
import com.trendyol.shoppingcart.model.rest.RestResponse;
import com.trendyol.shoppingcart.service.CampaignService;
import com.trendyol.shoppingcart.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/coupon")
public class CouponController {
    @Autowired
    private CouponService couponService;

    @PostMapping("/add")
    public RestResponse<Boolean> addCoupon(@RequestBody Coupon coupon) {
        boolean response = couponService.addCoupon(coupon);
        return new RestResponse<>(response);
    }
}
