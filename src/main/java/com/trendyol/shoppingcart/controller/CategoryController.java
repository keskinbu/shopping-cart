package com.trendyol.shoppingcart.controller;

import com.trendyol.shoppingcart.model.Category;
import com.trendyol.shoppingcart.model.rest.RestResponse;
import com.trendyol.shoppingcart.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping("/add")
    public RestResponse<Boolean> addCategory(@RequestBody Category category) {
        boolean response = categoryService.addCategory(category);
        return new RestResponse<>(response);
    }
}
