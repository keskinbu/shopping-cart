package com.trendyol.shoppingcart.controller;

import com.trendyol.shoppingcart.model.Product;
import com.trendyol.shoppingcart.model.rest.RestResponse;
import com.trendyol.shoppingcart.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    public RestResponse<List<Product>> getProducts() {
        List<Product> response = productService.getProducts();
        return new RestResponse<>(response);
    }

    @PostMapping("/add")
    public RestResponse<Boolean> addProduct(@RequestBody Product product) {
        boolean response = productService.addProduct(product);
        return new RestResponse<>(response);
    }
}
