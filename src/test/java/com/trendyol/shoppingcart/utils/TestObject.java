package com.trendyol.shoppingcart.utils;

import com.trendyol.shoppingcart.model.*;
import com.trendyol.shoppingcart.model.dto.ItemDto;

import java.math.BigDecimal;

public class TestObject {
    public static Category getCategory() {
        return new Category("Giyim");
    }

    public static Category getCategoryWithParent() {
        return new Category("T-Shirt", getCategory());
    }

    public static Category getCategoryDifferentCategoryWithoutParent() {
        return new Category("Ayakkabi");
    }

    public static Product getProduct() {
        return new Product("Siyah T-Shirt", new BigDecimal(40.00), getCategoryWithParent());
    }

    public static Product getProductSameCategory() {
        return new Product("Beyaz T-Shirt", new BigDecimal(30.00), getCategoryWithParent());
    }

    public static Product getProductDifferentCategory() {
        return new Product("Spor Ayakkabi", new BigDecimal(120.00), getCategoryDifferentCategoryWithoutParent());
    }

    public static Item getItem() {
        return new Item(getProduct(), 5);
    }

    public static ItemDto getItemDto() {
        ItemDto itemDto = new ItemDto();
        itemDto.setCartId(null);
        itemDto.setProductId(1L);
        itemDto.setProductCount(5);
        return itemDto;
    }

    public static Item getDifferentItem() {
        return new Item(getProductSameCategory(), 2);
    }

    public static Cart getCart() {
        Cart cart = new Cart();

        cart.addItem(getItem());

        cart.addItem(getDifferentItem());

        return cart;
    }

    public static Campaign getCampaignWithRate10() {
        return new Campaign(getCategory(), new BigDecimal("10.00"), DiscountType.RATE, 3);
    }

    public static Campaign getCampaignWithRate50() {
        return new Campaign(getCategory(), new BigDecimal("50.00"), DiscountType.RATE, 5);
    }

    public static Campaign getCampaignWithPrice() {
        return new Campaign(getCategory(), new BigDecimal("20.00"), DiscountType.PRICE, 2);
    }

    public static Cart applyCampaigns() {
        Cart cart = getCart();

        // todo apply campaigns.

        return cart;
    }

    public static Cart applyCoupons() {
        Cart cart = getCart();

        // todo apply coupons.

        return cart;
    }

//    public static double getDeliveryCost()
//    {
//        Cart cart = getCart();
//
////        cart.g
//    }
}
