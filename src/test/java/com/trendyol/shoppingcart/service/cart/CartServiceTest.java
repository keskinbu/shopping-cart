package com.trendyol.shoppingcart.service.cart;

import com.trendyol.shoppingcart.dao.CartRepository;
import com.trendyol.shoppingcart.dao.ProductRepository;
import com.trendyol.shoppingcart.model.Cart;
import com.trendyol.shoppingcart.service.CampaignService;
import com.trendyol.shoppingcart.service.CartService;
import com.trendyol.shoppingcart.service.CouponService;
import com.trendyol.shoppingcart.service.DeliveryService;
import com.trendyol.shoppingcart.service.impl.CartServiceImpl;
import com.trendyol.shoppingcart.utils.TestObject;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CartServiceTest {
    private CartService cartService;
    private ProductRepository productRepository;
    private CartRepository cartRepository;
    private DeliveryService deliveryService;
    private CouponService couponService;
    private CampaignService campaignService;

    @Before
    public void setup() {
        this.productRepository = mock(ProductRepository.class);
        this.cartRepository = mock(CartRepository.class);
        this.deliveryService = mock(DeliveryService.class);
        this.couponService = mock(CouponService.class);
        this.campaignService = mock(CampaignService.class);

        this.cartService = new CartServiceImpl(this.deliveryService, this.campaignService, this.couponService, this.cartRepository, this.productRepository);
    }

    @Test
    public void givenCartAndCheckTotalAmount() {
        when(productRepository.findById(TestObject.getItemDto().getProductId())).thenReturn(Optional.of(TestObject.getProduct()));

        Cart cart = cartService.addItem(TestObject.getItemDto());

        assertThat(cart.getTotalAmount()).isEqualTo(TestObject.getItem().getProduct().getPrice().multiply(new BigDecimal(TestObject.getItem().getQuantity())));
    }
}
