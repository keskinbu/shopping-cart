package com.trendyol.shoppingcart.util;

import com.trendyol.shoppingcart.model.Cart;
import com.trendyol.shoppingcart.utils.TestObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;

public class DeliveryCostCalculatorTest {
    private DeliveryCostCalculator deliveryCostCalculator;

    @Before
    public void setup() {
        BigDecimal costPerDelivery = new BigDecimal("5.00");
        BigDecimal costPerProduct = new BigDecimal("2.00");
        BigDecimal fixedCost = new BigDecimal("2.99");

        this.deliveryCostCalculator = new DeliveryCostCalculator(
                costPerDelivery,
                costPerProduct,
                fixedCost
        );
    }

    @Test
    public void testCalculateCost() {
        BigDecimal cost = this.deliveryCostCalculator.calculateCost(2, 4);

        Assertions.assertEquals(cost, new BigDecimal("20.99"));
    }

    @Test
    public void testCalculateFor() {
        Cart cart = TestObject.getCart();

        this.deliveryCostCalculator.calculateFor(cart);

        Assertions.assertEquals(cart.getDeliveryCost(), new BigDecimal("9.99"));
    }
}